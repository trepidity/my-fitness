# Overview
This page is a list of resources that have helped me to be healthy or become healthy. Some are for the seasoned lifter, and some are for those that just want to start out.

## Getting Started
Find your one single reason to start.
Here is a list of options, pick only one.
* Stop eating chips
* No seconds
* Prepare your lunches
* Always have a healthy snack, almonds or soybeans.
* No soda
* Drink at least your body weight in water (oz)
* No deep-fried food

## Get more movement into your life.
Do one of the following every day. (None of this requires weights)

Do at least 50 reps, doesn't have to be at one time or at least 15 minutes worth.
* Push-ups
* Pull-ups
* Squats

## More advanced things. 
These are things to do after a month or two.
* Log all your food in myfitnesspal.
* Limit your calorie intake.
* Add Weights to your workout.


# Resources
[Jocko Willink - Discipline Equals Freedom](https://www.amazon.com/Discipline-Equals-Freedom-Field-Manual/dp/1250156947)
[Simple Science Fitness] (http://ss.fitness)

